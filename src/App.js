// Import Redux
import { Provider } from 'react-redux'
// Import Store
import store from './store'
// Import Main SCSS
import './sass/main.scss'
// Import Components
import Tasks from './components/ui/Tasks'



function App() {

	return (
		<Provider store={store} >
			<div className="container container--general">
				<div className="container--limit">
					<Tasks />
				</div>
			</div>
		</Provider>
	)

}



export default App