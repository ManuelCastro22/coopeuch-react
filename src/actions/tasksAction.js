/* TASKS ACTION */


// Import types
import {
	CREATE_TASK,
	EDIT_TASK,
	CHECK_TASK,
	DELETE_TASK,
	CANCEL_EDIT_TASK,
	UPDATE_TASK
} from '../types'


/* Functions used in the views */
export function createTaskAction(task) {
	return dispatch => dispatch( updateTasks(task) )
}
// Update task to do
const updateTasks = task => ({
	type: CREATE_TASK,
	payload: task
})


export function deleteCurrentTaskAction(taskId) {
	return dispatch => dispatch( deleteCurrentTask(taskId) )
}
// Delete current task
const deleteCurrentTask = taskId => ({
	type: DELETE_TASK,
	payload: taskId
})


export function checkCurrentTaskAction(taskId) {
	return dispatch => dispatch( checkCurrentTask(taskId) )
}
// Check current task
const checkCurrentTask = taskId => ({
	type: CHECK_TASK,
	payload: taskId
})


export function editCurrentTaskAction(taskId) {
	return dispatch => dispatch( editCurrentTask(taskId) )
}
// Check current task
const editCurrentTask = taskId => ({
	type: EDIT_TASK,
	payload: taskId
})


export function cancelEditTaskAction() {
	return dispatch => dispatch( cancelEditTask() )
}
// Check current task
const cancelEditTask = () => ({
	type: CANCEL_EDIT_TASK
})


export function updateTaskAction(task) {
	return dispatch => dispatch( updateTask(task) )
}
// Update task
const updateTask = task => ({
	type: UPDATE_TASK,
	payload: task
})