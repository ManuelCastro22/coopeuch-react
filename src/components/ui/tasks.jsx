/* TASKS */

// Import React
import React from 'react'
// Import Components
import TasksForm from './TasksForm'
import TasksList from './TasksList'
// Import SCSS
import '../../sass/components/_tasks.scss'
import '../../sass/components/_buttons.scss'



const Tasks = () => {
	return (
		<section className="tasks">
			<h1 className="tasks__title">
				Organizador de tareas
			</h1>
			<header className="tasks__header">
				<TasksForm />
			</header>
			<div className="tasks__container">
				<TasksList
					type="toDo" />
				<TasksList
					additionalClass="tasks__list--done"
					type="done" />
			</div>
		</section>
	)
}
 


export default Tasks