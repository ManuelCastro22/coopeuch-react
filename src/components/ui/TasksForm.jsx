/* TASKS */

// Import React
import React, { useEffect } from 'react'
// Import Redux Hooks
import { useDispatch, useSelector } from 'react-redux'
// Import Actions
import { createTaskAction,
	updateTaskAction,
	cancelEditTaskAction
} from '../../actions/tasksAction'
// Import uuid
import { v4 as uuid } from 'uuid'
// Import React Hook Form
import { useForm } from 'react-hook-form'
// Import SCSS
import '../../sass/components/_form.scss'



const TasksForm = () => {


	// Use selector
	const tasksData = useSelector( state => state.tasks )


	// Use dispatch
	const dispatch = useDispatch()


	// Destructuring
	const { isEditing, allTasks } = tasksData


	// Config React Hook Form
	const { register, handleSubmit, formState: { errors }, setValue  } = useForm()


	// Functions
	const onSubmit = data => {
		let task = {
			id: uuid(),
			text: data.textTask,
			edit: false,
			checked: false
		}

		if( isEditing.state ) {
			task = {
				...task,
				id: isEditing.id,
			}
			dispatch( updateTaskAction(task) )
		} else dispatch( createTaskAction(task) )

		setValue( 'textTask', '' )
	}

	
	// Use Effect
	useEffect( () => {
		isEditing.id ? setTextToEdit() : setValue('textTask', '')
	}, [isEditing.id] )


	// Set text to edit
	function setTextToEdit() {
		const currentTask = allTasks.find( task => task.id === isEditing.id )
		setValue( 'textTask', currentTask.text )
	}


	// Validation
	const validation = {
		required: 'Campo obligatorio',
		minLength: {
			value: 10,
			message: 'La tarea debe tener mínimo 10 caracteres'
		}
	}


	// Handle cancel edit
	const handleCancelEdit = () => dispatch( cancelEditTaskAction() )


	return (
		<form
			className="form"
			onSubmit={ handleSubmit(onSubmit) } >
			<div className="form__box">
				<input
					className="form__textarea"
					id="textTask"
					name="textTask"
					{ ...register('textTask', validation)}
					placeholder="¿Que vas a hacer?" />
				{ errors['textTask'] && (
					<p className="form__error">{ errors['textTask'].message }</p>
				) }
			</div>
			<div className="form__box">
				<button
					type="submit"
					className="btn" >
						{ isEditing.state ? 'Editar' : 'Crear' }
				</button>
				{ isEditing.state && (
					<button
						onClick={ () => handleCancelEdit() }
						className="btn btn--red ml10"
						type="button" >
						Cancelar
					</button>
				) }
			</div>
		</form>
	)
}
 


export default TasksForm