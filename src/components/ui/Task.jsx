/* TASK */

// Import React
import React from 'react'
// Import Redux Hooks
import { useDispatch } from 'react-redux'
// Import Actions
import { deleteCurrentTaskAction,
	cancelEditTaskAction,
	checkCurrentTaskAction,
	editCurrentTaskAction
} from '../../actions/tasksAction'



const Task = ({ taskData }) => {


	// Dispatch
	const dispatch = useDispatch()

	
	// Destructuring
	const { id, text, checked } = taskData


	// Handle delete task
	const handleDeleteTask = taskId => { 
		dispatch( cancelEditTaskAction() )
		dispatch( deleteCurrentTaskAction(taskId) )
	}
	
	// Handle check task
	const handleCheckTask = taskId => dispatch( checkCurrentTaskAction(taskId) )
	
	// Handle edit task
	const handleEditTask = taskId => {
		window.scrollTo({
			top: 0,
			left: 0,
			behavior: 'smooth'
		})
		dispatch( editCurrentTaskAction(taskId) )
	}


	return (
		<li className="task">
			<span
				onClick={ () => handleCheckTask(id) }
				className={ `task__state ${ checked ? 'checked' : '' }` } />
			{ text && (
				<p className="task__text">
					{ text }
				</p>
			) }
			<span 
				onClick={ () => handleEditTask(id) }
				className="task__edit" />
			<span
				onClick={ () => handleDeleteTask(id) }
				className="task__delete" />
		</li>
	)
}
 


export default Task