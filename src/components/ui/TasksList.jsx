/* TASKS LIST */

// Import React and Hooks
import React from 'react'
// Import Redux Hooks
import { useSelector } from 'react-redux'
// Import Components
import Task from './Task'



const TasksList = ({ additionalClass, type }) => {


	// Use selector
	const tasksData = useSelector( state => state.tasks )


	// Destructuring
	const { allTasks } = tasksData


	return (
		<ul className={`tasks__list ${ additionalClass !== undefined ? additionalClass : '' }`}>
			{ type === 'toDo' && (
				<>
					<h2 className="tasks__list__title"> Tareas por hacer </h2>
					{ allTasks.length > 0 && allTasks.map( task => {
						if( task.checked === false)
							return <Task
								key={ task.id }
								taskData={ task } />
					} ) }
				</>
			) }
			{ type === 'done' && (
				<>
					<h2 className="tasks__list__title"> Tareas hechas </h2>
					{ allTasks.length > 0 && allTasks.map( task => {
						if( task.checked )
							return <Task
								key={ task.id }
								taskData={ task } />
					} )}
				</>
			) }
		</ul>
	)
}
 


export default TasksList