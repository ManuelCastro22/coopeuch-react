/* MAIN REDUCER */

// Import Redux
import {
    combineReducers
} from 'redux'
// Import Reducers
import tasksReducer from '../reducers/tasksReducer'



// State general
export default combineReducers({
    tasks: tasksReducer
})