/* TASKS REDUCER */


// Import types
import {
	CREATE_TASK,
	EDIT_TASK,
	UPDATE_TASK,
	CHECK_TASK,
	CANCEL_EDIT_TASK,
	DELETE_TASK
} from '../types'



// State of Reducer
const initialState = {
	allTasks: [],
	isEditing: {
		state: false,
		id: null
	}
}



export default ( state = initialState, action ) => {
	switch( action.type ) {
		case CREATE_TASK:
			return {
				...state,
				allTasks: [
					action.payload,
					...state.allTasks
				]
			}
		case EDIT_TASK:
			return {
				...state,
				isEditing: {
					state: true,
					id: action.payload
				},
				allTasks: state.allTasks.map( task => {
					return {
						...task,
						edit: task.id === action.payload ? true : task.checked
					}
				} )
			}
		case UPDATE_TASK:
			return {
				...state,
				isEditing: {
					state: false,
					id: null
				},
				allTasks: state.allTasks.map( task => {
					return {
						...task,
						text: task.id === action.payload.id ? action.payload.text : task.text
					}
				} )
			}
		case CHECK_TASK:
			return {
				...state,
				allTasks: state.allTasks.map( task => {
					return {
						...task,
						checked: task.id === action.payload ? !task.checked : task.checked
					}
				} )
			}
		case CANCEL_EDIT_TASK:
			return {
				...state,
				isEditing: {
					state: false,
					id: null
				},
			}
		case DELETE_TASK:
			return {
				...state,
				allTasks: state.allTasks.filter( task => task.id !== action.payload )
			}
		default:
			return state
	}
}