/* TYPES */


// TASKS
export const CREATE_TASK = 'CREATE_TASK'
export const UPDATE_TASK = 'UPDATE_TASK'
export const CHECK_TASK = 'CHECK_TASK'
export const DELETE_TASK = 'DELETE_TASK'
export const EDIT_TASK = 'EDIT_TASK'
export const SUCCESS_EDIT_TASK = 'SUCCESS_EDIT_TASK'
export const CANCEL_EDIT_TASK = 'CANCEL_EDIT_TASK'